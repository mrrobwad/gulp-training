var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var mainBowerFiles = require('main-bower-files');
var sh = require('shelljs');
var runSequence = require('run-sequence');
var del = require('del');
var vinylPaths = require('vinyl-paths');
var cache = require('gulp-cached');
var sass = require('gulp-sass');
var cssnano = require('gulp-cssnano');
var rename = require('gulp-rename');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');
var uglify = require('gulp-uglify');
var notify = require('gulp-notify');
var browserSync = require('browser-sync');
var reload = browserSync.reload;

// Configure project
var config = {
    bowerPath: './bower_components',
    sassPath: './src/scss/*.scss',
    scssPath: './src/scss/custom.scss',
    jsPath: './src/js/custom.js',
    imgPath: './src/img/**/*',
    distPath: './dist',
    distFiles: ['./index.html', './robots.txt', './web.config']
}

// Default task, just runs install and setup task(s)
gulp.task('default', ['build']);

// gulp build - performs necessary build operations
gulp.task('build', ['install'], function (done) {
    runSequence('clean', 'dist', done);
});

// gulp clean - clears dist folder for fresh build
gulp.task('clean', function () {
    return gulp.src(config.distPath)
        .pipe(vinylPaths(del));
});

// gulp dist - processes and copies files to dist folder
gulp.task('dist', ['bower', 'sass', 'js', 'img', 'distFiles']);

// gulp dist - processes and copies files to dist folder
gulp.task('distFiles', function () {
    return gulp.src(config.distFiles)
        .pipe(gulp.dest(config.distPath));
});

// gulp bower - retrieves bower libs and copies them to dist directory
// overrides in bower.json
gulp.task('bower', function () {
    return gulp.src(mainBowerFiles(), {
            base: 'bower_components'
        })
        .pipe(cache('bower'))
        .pipe(gulp.dest(config.distPath + '/lib'));
});

// gulp sass - compiles sass, minifies, and creates .min.css dist file
gulp.task('sass', function () {
    return gulp.src(config.scssPath)
        .pipe(cache('scss'))
        .pipe(sass())
        .on('error', sass.logError)
        .pipe(cssnano())
        .pipe(rename({
            extname: '.min.css'
        }))
        .pipe(gulp.dest(config.distPath + '/css/'));
});

// gulp js - lints javascript, minifies, and creates .min.js dist file
gulp.task('js', function () {
    return gulp.src(config.jsPath)
        .pipe(cache('js'))
        .pipe(jshint())
        .pipe(jshint.reporter(stylish))
        .pipe(jshint.reporter('fail'))
        .on('error', function (e) {
            console.log(e.toString());
            this.emit('end');
        })
        .pipe(uglify())
        .pipe(rename({
            extname: '.min.js'
        }))
        .pipe(gulp.dest(config.distPath + '/js/'));
});

// gulp img - optimizes images and copies to dist folder
gulp.task('img', function () {
    return gulp.src(config.imgPath)
        .pipe(cache('img'))
        //.pipe(imagemin())
        .pipe(gulp.dest(config.distPath + '/img/'));
});

// gulp install - performs bower install
gulp.task('install', ['git-check'], function () {
    return bower.commands.install()
        .on('log', function (data) {
            gutil.log('bower', gutil.colors.cyan(data.id), data.message);
        });
});

// gulp serve - launch local server
gulp.task('serve', function () {
    runSequence('build', 'watch', 'create-server', function () {
        gulp.src(config.distPath)
            .pipe(notify("Started Gulp Training Server!"));
    });
});

// gulp create-server - instantiates browser-sync server
gulp.task('create-server', function () {
    browserSync({
        server: {
            baseDir: config.distPath
        }
    });

    gulp.watch(config.distPath + '/**/*', reload);
});

// gulp watch - watch scss, js, images, and bower libs for changes and process them
gulp.task('watch', function () {
    gulp.watch(config.sassPath, ['sass']);
    gulp.watch(config.jsPath, ['js']);
    gulp.watch(config.imgPath, ['img']);
    gulp.watch(config.bowerPath + '/**', ['bower']);
    gulp.watch(config.distFiles, ['distFiles']);
});

// gulp git-check - utility to verify git is present
gulp.task('git-check', function (done) {
    if (!sh.which('git')) {
        console.log(
            '  ' + gutil.colors.red('Git is not installed.'),
            '\n  Git, the version control system, is required for this project.',
            '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
            '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
        );
        process.exit(1);
    }
    done();
});
